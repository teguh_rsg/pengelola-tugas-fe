"use client"

import instanceAxios from '@/axios/instance'
import useSWRMutation from 'swr/mutation'
import { useForm } from "react-hook-form";
import { useRouter } from "next/navigation";
import { useEffect } from "react";
import { toast } from 'react-toastify';

type FormValues = {
    email: string;
    password: string;
};

export default function Login() {
    const router = useRouter()

    useEffect(function () {
        if (window.localStorage.getItem("jwtToken")) {
            router.push('/')
        }
    }, [])
    //POST
    async function sendReq(url:string, data:{ arg: string }) {
        const post = await instanceAxios.post(url, data?.arg).then(res => res.data)
            .catch(function (err) {
                toast.error('Error ' + err.response?.status + ' | ' + err.response?.data?.message)
            })
        if (post?.token) {
            window.localStorage.setItem("jwtToken", post.token)
            toast.success('Selamat datang')
            router.push('/')
        } else {
            toast.error('Gagal Login. Token tidak ditemukan.')
        }
    }
    const address = 'login';
    const { trigger, isMutating } = useSWRMutation(address, sendReq)

    const { register, handleSubmit, formState: { errors } } = useForm<FormValues>();

    const onSubmit = (data:any) => trigger(data)

    return (
        <div className="min-h-screen">
            <div className="flex flex-row justify-center p-2">
                <div className="md:basis-96 grow md:grow-0 mt-9 p-4 border border-slate-50 rounded-md shadow-lg">
                    <h1 className='text-4xl font-bold'>Login</h1>
                    <form className='mt-4' onSubmit={handleSubmit(onSubmit)}>
                        <div className="form-control w-full">
                            <label className="label">
                                <span className="label-text">email:</span>
                            </label>
                            <input {...register("email")} required type="email" placeholder="Email" className="input input-bordered w-full" />
                        </div>
                        <div className="form-control w-full">
                            <label className="label">
                                <span className="label-text">Password:</span>
                            </label>
                            <input {...register("password")} required type="password" placeholder="Password" className="input input-bordered w-full" />
                        </div>

                        <div className="form-control w-full">
                            {isMutating ? 'Loading...' : <button className="btn btn-primary btn-sm my-3">Login</button>}
                        </div>

                    </form>
                </div>
            </div>
        </div>
    )
}
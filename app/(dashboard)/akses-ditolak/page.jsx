import Link from "next/link";

export default function AksesDitolak() {
    return (
        <>
            <h1 className="text-2xl font-bold leading-7">Akses Ditolak</h1>

            <Link href="/" className="btn btn-neutral btn-sm m-2">Home</Link>
        </>
    )
}
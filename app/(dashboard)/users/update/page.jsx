"use client"

import instanceAxios from "@/axios/instance";
import { useForm } from "react-hook-form";
import { useRouter, useSearchParams } from "next/navigation"
import useSWRMutation from 'swr/mutation'
import useSWR from 'swr'
import { toast } from "react-toastify";

export default function Edit(){
    const searchParams = useSearchParams()
    const id = searchParams.get('id')
    const router = useRouter()

    //PUT
    async function sendReq(url, data){
        const put = await instanceAxios.put(url, data?.arg).then(res => res.data)
        .catch(function(err){
            toast.error(err?.response?.data?.message)
        })
        console.log(url, data)
        if(put){
            toast.success(put?.message)
            router.push('/users')    
        }
    }
    const urlPut = 'admin/user/'+id;
    const { trigger, isMutating } = useSWRMutation(urlPut, sendReq)
    
    const { register, handleSubmit, formState: { errors } } = useForm();

    const onSubmit = data => trigger(data)
    //END PUT
    
    //GET
    const fetcher = (url) => instanceAxios.get(url).then(res => res.data)
    const address = 'admin/user/' + id;
    const { data, error } = useSWR(address, fetcher);
    if (error){
        return <p>Loading failed...</p>;
    }
    if (!data){
        return <h1>Loading...</h1>;
    }
    //END GET

    return (
        <div className="flex">
            <div className="w-full max-w-xs">
                <h1 className="text-2xl font-bold leading-7">Update Data User</h1>

                <form onSubmit={handleSubmit(onSubmit)}>
                    <div className="form-control">
                        <label className="label">
                            <span className="label-text">*Username:</span>
                        </label>
                        <input defaultValue={data?.username} {...register("username", { required: true })}
                            type="text" placeholder="Username" className="input input-bordered" />
                        {errors.username?.type === 'required' && <span className="text-red-600">Wajib diisi</span>}
                    </div>

                    <div className="form-control">
                        <label className="label">
                            <span className="label-text">*Email:</span>
                        </label>
                        <input defaultValue={data?.email} {...register("email", { required: true })}
                            type="email" placeholder="email" className="input input-bordered" />
                        {errors.email?.type === 'required' && <span className="text-red-600">Wajib diisi</span>}
                    </div>

                    {isMutating ? 'Updating...' : <input className="btn btn-neutral btn-sm m-2" type="submit" />}
                </form>
            </div>
        </div>
    )
}
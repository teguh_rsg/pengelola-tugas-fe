"use client"

import instanceAxios from "@/axios/instance"
import useSWRMutation from 'swr/mutation'
import { useForm } from "react-hook-form";
import { useRouter } from "next/navigation";
import { toast } from "react-toastify";

export default function Create() {
    const router = useRouter()
    //POST
    async function sendReq(url, data) {
        const post = await instanceAxios.post(url, data?.arg).then(res => res.data)
            .catch(function (err) {
                console.log(err)
                toast.error(err?.response?.data?.message)
            })
        console.log(url, data)
        if(post){
            toast.success(post?.message)
            router.push('/users')
        }
    }
    const address = 'admin/user';
    const { trigger, isMutating } = useSWRMutation(address, sendReq)

    const { register, handleSubmit, formState: { errors } } = useForm();

    const onSubmit = data => trigger(data)
    //END POST

    return (
        <div className="flex">
            <div className="w-full max-w-xs">
                <h1 className="text-2xl font-bold leading-7">Tambah User Baru</h1>

                <form onSubmit={handleSubmit(onSubmit)}>
                    <div className="form-control">
                        <label className="label">
                            <span className="label-text">*Username:</span>
                        </label>
                        <input {...register("username", { required: true })}
                            type="text" placeholder="Username" className="input input-bordered" />
                        {errors.username?.type === 'required' && <span className="text-red-600">Wajib diisi</span>}
                    </div>

                    <div className="form-control">
                        <label className="label">
                            <span className="label-text">*Email:</span>
                        </label>
                        <input {...register("email", { required: true })}
                            type="email" placeholder="email" className="input input-bordered" />
                        {errors.email?.type === 'required' && <span className="text-red-600">Wajib diisi</span>}
                    </div>

                    <div className="form-control">
                        <label className="label">
                            <span className="label-text">*Password:</span>
                        </label>
                        <input {...register("password", { required: true })}
                            type="password" placeholder="password" className="input input-bordered" />
                        {errors.password?.type === 'required' && <span className="text-red-600">Wajib diisi</span>}
                    </div>

                    {isMutating ? 'Sending...' : <input className="btn btn-neutral btn-sm m-2" type="submit" />}
                </form>
            </div>
        </div>
    )
}
"use client"

import instanceAxios from "@/axios/instance";
import { useSearchParams } from "next/navigation"
import useSWRMutation from 'swr/mutation'
import useSWR, { useSWRConfig } from 'swr'
import { toast } from "react-toastify";
import { useForm } from "react-hook-form";
import Swal from "sweetalert2";

export default function Groups() {
    const searchParams = useSearchParams()
    const id = searchParams.get('id')

    //GET
    const fetcher = (url) => instanceAxios.get(url).then(res => res.data)
    const address = 'admin/user/' + id;
    const { data, error } = useSWR(address, fetcher);
    if (error) {
        return <p>Loading failed...</p>;
    }
    if (!data) {
        return <h1>Loading...</h1>;
    }
    //END GET

    return (
        <div className="p-2">
            <div className="flex">
                <div className="w-full max-w-xs">
                    <h1 className="text-2xl font-bold leading-7">Hak Akses</h1>
                    <p>ID : {data.id}</p>
                    <p>Username : {data.username}</p>
                    <p>Email : {data.email}</p>
                </div>
            </div>

            <h2 className="text-l font-bold leading-7 mt-1">Hak Akses User:</h2>

            <ListHakAkses userId={id} />

            <h2 className="text-2xl font-bold leading-7 mt-10">Tambah Hak Akses</h2>

            <TambahHakAkses userId={id} />
        </div>
    )
}

function ListHakAkses({ userId }) {
    //GET LIST
    const fetcher = (url) => instanceAxios.get(url).then(res => res.data)
    const address = 'admin/grup-by-user/' + userId;
    const { data, error } = useSWR(address, fetcher);
    if (error) {
        return <p>Loading failed...</p>;
    }
    if (!data) {
        return <h1>Loading...</h1>;
    }
    //END GET LIST
    return data.map((d) => <ButtonHapus key={d.id} keterangan={d.name} idData={d.id} />)
}

function TambahHakAkses({ userId }) {
    const { mutate } = useSWRConfig()

    //POST
    async function sendReq(url, data) {
        const post = await instanceAxios.post(url, data?.arg).then(res => res.data)
            .catch(function (err) {
                console.log(err)
                toast.error(err?.response?.data?.message)
            })
        console.log(url, data)
        if (post) {
            toast.success(post?.message)
            mutate('admin/grup-by-user/' + userId)
        }
    }
    const addressPost = 'admin/user-grup';
    const { trigger, isMutating } = useSWRMutation(addressPost, sendReq)

    const { register, handleSubmit, formState: { errors } } = useForm();

    const onSubmit = data => trigger(data)
    //END POST

    //GET LIST
    const fetcher = (url) => instanceAxios.get(url).then(res => res.data)
    const address = 'list-option/grup';
    const { data, error } = useSWR(address, fetcher);
    if (error) {
        return <p>Loading failed...</p>;
    }
    if (!data) {
        return <h1>Loading...</h1>;
    }
    // end get

    return (
        <form onSubmit={handleSubmit(onSubmit)} className="max-w-xs">
            <input type="hidden" defaultValue={userId} {...register("user_id")} />
            <div className="form-control">
                <label className="label">
                    <span className="label-text">*Hak Akses:</span>
                </label>
                <select className="input input-bordered" {...register("grup_id", { required: true })}>
                    <option value="">Pilih..</option>
                    {data?.data.map((d) => <option value={d.id} key={d.id}>{d.name}</option>)}
                </select>
                {errors.grup_id?.type === 'required' && <span className="text-red-600">Wajib diisi</span>}
            </div>

            {isMutating ? 'Sending...' : <input className="btn btn-neutral btn-sm m-2" type="submit" />}
        </form>
    )
}

function ButtonHapus({ keterangan, idData }) {
    const searchParams = useSearchParams()
    const { mutate } = useSWRConfig()

    function confirm() {
        Swal.fire({
            title: 'Delete?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.isConfirmed) {
                trigger()
            }
        })
    }

    async function sendReq(url) {
        const result = await instanceAxios.delete(url).then(res => res.data)
            .catch(function (err) {
                toast.error(err?.response?.data?.message)
            })
        console.log(url)
        if (result) {
            toast.success(result?.message)
            mutate('admin/grup-by-user/' + searchParams.get('id'))
        }
    }
    const addressDel = 'admin/user-grup/' + idData;
    const { trigger, isMutating } = useSWRMutation(addressDel, sendReq)

    if (isMutating) {
        return 'deleting...'
    }

    return <div className="badge badge-neutral badge-lg gap-2 m-1">
        {keterangan}
        <button onClick={confirm}>
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" className="inline-block w-4 h-4 stroke-current"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M6 18L18 6M6 6l12 12"></path></svg>
        </button>
    </div>
}
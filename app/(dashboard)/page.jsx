'use client'

import instanceAxios from "@/axios/instance";
import useSWRImmutable from "swr/immutable";

export default function Home() {
    const fetcher = (url) => instanceAxios.get(url).then(res => res.data)
    const address = 'general/ambil-feed';
    const { data, error } = useSWRImmutable(address, fetcher);

    function ubahTanggal(teks){
        const tanggal = Date.parse(teks)
        const t = new Date(tanggal)
        return t.toLocaleDateString()
    }

    if (error){
        return <p>Loading failed...</p>;
    }
    if (!data){
        return <h1>Loading...</h1>;
    }
    
    return (
        <>
            <h1 className="text-2xl font-bold leading-7 m-3 mb-5">Pengumuman</h1>

            <div className="grid grid-cols-1 md:grid-cols-2 gap-4">

                {data?.data.map((d) => (
                    <div key={d.guid} className="card bg-base-100 shadow-xl">
                        <div className="card-body">
                            <h2 className="card-title">{typeof d.title === 'string' ? d.title : ''}</h2>
                            <p>
                                <i>{ubahTanggal(d.pubDate)}</i>
                                <br/>
                                {typeof d.description === 'string' ? d.description : ''}
                            </p>
                            <div className="card-actions justify-end">
                                <a target="_blank" href={d.link} className="btn btn-primary" rel="noopener noreferrer">Baca Selengkapnya</a>
                            </div>
                        </div>
                    </div>
                ))}
            </div>            
        </>
    )
}
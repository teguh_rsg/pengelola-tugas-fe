"use client"

import instanceAxios from "@/axios/instance";
import { Controller, useForm } from "react-hook-form";
import { useRouter, useSearchParams } from "next/navigation"
import useSWRMutation from 'swr/mutation'
import useSWR from 'swr'
import { toast } from "react-toastify";
import SelectAjax from "@/components/SelectAjax";

export default function Edit(){
    const searchParams = useSearchParams()
    const id = searchParams.get('id')
    const router = useRouter()

    //PUT
    async function sendReq(url, data){
        const put = await instanceAxios.put(url, data?.arg).then(res => res.data)
        .catch(function(err){
            toast.error(err?.response?.data?.message)
        })
        console.log(url, data)
        if(put){
            toast.success(put?.message)
            router.push('/tugas')    
        }
    }
    const urlPut = 'mahasiswa/tugas/'+id;
    const { trigger, isMutating } = useSWRMutation(urlPut, sendReq)
    
    const { register, handleSubmit, formState: { errors }, control } = useForm();

    const onSubmit = data => trigger(data)
    //END PUT
    
    //GET
    const fetcher = (url) => instanceAxios.get(url).then(res => res.data)
    const address = 'mahasiswa/tugas/' + id;
    const { data, error } = useSWR(address, fetcher);
    if (error){
        return <p>Loading failed...</p>;
    }
    if (!data){
        return <h1>Loading...</h1>;
    }
    //END GET

    return (
        <div>
            <h1 className="text-2xl font-bold leading-7">Update Tugas</h1>
            
            <form onSubmit={handleSubmit(onSubmit)}>
                <div className="grid grid-cols-1 md:grid-cols-2 gap-4">
                    <div className="form-control">
                        <label className="label">
                            <span className="label-text">*Jadwal:</span>
                        </label>
                        {/* <SelectJadwal defaultValue={data?.jadwal_id} register={register("jadwal_id", { required: true })}/>
                        {errors.jadwal_id?.type === 'required' && <span className="text-red-600">Wajib diisi</span>} */}
                        <Controller
                            rules={{ required: true }}
                            control={control}
                            name="jadwal_id"
                            defaultValue={data?.jadwal_id}
                            render={({ field }) => (
                                <SelectAjax 
                                    labelDefaultValue={data?.mata_kuliah}
                                    field={field}
                                    createAddress={(search)=> 'mahasiswa/jadwal?search=' + search}
                                    optionsMap = {(data) => data?.data?.map(d => ({
                                        value: d.id,
                                        label: d.mata_kuliah,
                                    }))}
                                />
                            )}
                        />
                        {errors.jadwal_id?.type === 'required' && <span className="text-red-600">Wajib diisi</span>}
                    </div>

                    <div className="form-control">
                        <label className="label">
                            <span className="label-text">*Judul:</span>
                        </label>
                        <input defaultValue={data?.judul} {...register("judul", { required: true })}
                            type="text" placeholder="Judul" className="input input-bordered" />
                        {errors.judul?.type === 'required' && <span className="text-red-600">Wajib diisi</span>}
                    </div>

                    <div className="form-control">
                        <label className="label">
                            <span className="label-text">*Batas Pengumpulan:</span>
                        </label>
                        <input defaultValue={data?.deadline} {...register("deadline", { required: true })}
                            type="datetime-local" placeholder="Batas Pengumpulan" className="input input-bordered" />
                        {errors.deadline?.type === 'required' && <span className="text-red-600">Wajib diisi</span>}
                    </div>

                    <div className="form-control">
                        <label className="label">
                            <span className="label-text">*Jenis:</span>
                        </label>
                        <select defaultValue={data?.jenis} className="input input-bordered" {...register("jenis", { required: true })}>
                            <option value="">Pilih..</option>
                            <option value="INDIVIDU">INDIVIDU</option>
                            <option value="KELOMPOK">KELOMPOK</option>
                        </select>
                        {errors.jenis?.type === 'required' && <span className="text-red-600">Wajib diisi</span>}
                    </div>

                    <div className="form-control">
                        <label className="label">
                            <span className="label-text">*Detail:</span>
                        </label>
                        <textarea defaultValue={data?.detail} {...register("detail", { required: true })}
                            placeholder="Detail" className="textarea-lg" />
                        {errors.detail?.type === 'required' && <span className="text-red-600">Wajib diisi</span>}
                    </div>
                </div>

                {isMutating ? 'Sending...' : <input className="btn btn-neutral btn-sm m-2" type="submit" />}
            </form>
        </div>
    )
}


function SelectJadwal(props) {
    //GET LIST
    const fetcher = (url) => instanceAxios.get(url).then(res => res.data)
    const address = 'mahasiswa/jadwal';
    const { data, error } = useSWR(address, fetcher);
    if (error) {
        return <p>Loading failed...</p>;
    }
    if (!data) {
        return <h1>Loading...</h1>;
    }
    
    return (
        <select defaultValue={props?.defaultValue} className="input input-bordered" {...props.register}>
                <option value="">Pilih..</option>
                {data?.data.map((d) => <option value={d.id} key={d.id}>{d.mata_kuliah} [semester {d.semester}]</option>)}
        </select>
    )
}
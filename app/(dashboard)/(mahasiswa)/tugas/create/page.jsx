"use client"

import instanceAxios from "@/axios/instance"
import useSWRMutation from 'swr/mutation'
import { Controller, useForm } from "react-hook-form";
import { useRouter } from "next/navigation";
import { toast } from "react-toastify";
import SelectAjax from "@/components/SelectAjax";

export default function Create() {
    const router = useRouter()
    //POST
    async function sendReq(url, data) {
        const post = await instanceAxios.post(url, data?.arg).then(res => res.data)
            .catch(function (err) {
                console.log(err)
                toast.error(err?.response?.data?.message)
            })
        console.log(url, data)
        if (post) {
            toast.success(post?.message)
            router.push('/tugas')
        }
    }
    const address = 'mahasiswa/tugas';
    const { trigger, isMutating } = useSWRMutation(address, sendReq)

    const {
        register,
        handleSubmit,
        formState: { errors },
        control,
        reset,
        setValue,
    } = useForm();

    const onSubmit = data => trigger(data)
    //END POST

    return (
        <div>

            <h1 className="text-2xl font-bold leading-7">Tambah Tugas Baru</h1>

            <form onSubmit={handleSubmit(onSubmit)}>
                <div className="grid grid-cols-1 md:grid-cols-2 gap-4">
                    <div className="form-control">
                        <label className="label">
                            <span className="label-text">*Jadwal:</span>
                        </label>
                        <Controller
                            rules={{ required: true }}
                            control={control}
                            name="jadwal_id"
                            render={({ field }) => (
                                <SelectAjax 
                                    field={field}
                                    createAddress={(search)=> 'mahasiswa/jadwal?search=' + search}
                                    optionsMap = {(data) => data?.data?.map(d => ({
                                        value: d.id,
                                        label: d.mata_kuliah,
                                    }))}
                                />
                            )}
                        />
                        {errors.jadwal_id?.type === 'required' && <span className="text-red-600">Wajib diisi</span>}
                    </div>

                    <div className="form-control">
                        <label className="label">
                            <span className="label-text">*Judul:</span>
                        </label>
                        <input {...register("judul", { required: true })}
                            type="text" placeholder="Judul" className="input input-bordered" />
                        {errors.judul?.type === 'required' && <span className="text-red-600">Wajib diisi</span>}
                    </div>

                    <div className="form-control">
                        <label className="label">
                            <span className="label-text">*Batas Pengumpulan:</span>
                        </label>
                        <input {...register("deadline", { required: true })}
                            type="datetime-local" placeholder="Batas Pengumpulan" className="input input-bordered" />
                        {errors.deadline?.type === 'required' && <span className="text-red-600">Wajib diisi</span>}
                    </div>

                    <div className="form-control">
                        <label className="label">
                            <span className="label-text">*Jenis:</span>
                        </label>
                        <select className="input input-bordered" {...register("jenis", { required: true })}>
                            <option value="">Pilih..</option>
                            <option value="INDIVIDU">INDIVIDU</option>
                            <option value="KELOMPOK">KELOMPOK</option>
                        </select>
                        {errors.jenis?.type === 'required' && <span className="text-red-600">Wajib diisi</span>}
                    </div>

                    <div className="form-control">
                        <label className="label">
                            <span className="label-text">*Detail:</span>
                        </label>
                        <textarea {...register("detail", { required: true })}
                            placeholder="Detail" className="textarea-lg" />
                        {errors.detail?.type === 'required' && <span className="text-red-600">Wajib diisi</span>}
                    </div>
                </div>

                {isMutating ? 'Sending...' : <input className="btn btn-neutral btn-sm m-2" type="submit" />}
            </form>
        </div>
    )
}

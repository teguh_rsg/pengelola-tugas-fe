"use client"

import instanceAxios from "@/axios/instance";
import { useForm } from "react-hook-form";
import { useRouter, useSearchParams } from "next/navigation"
import useSWRMutation from 'swr/mutation'
import useSWR from 'swr'
import { toast } from "react-toastify";

export default function Edit(){
    const searchParams = useSearchParams()
    const id = searchParams.get('id')
    const router = useRouter()

    //PUT
    async function sendReq(url, data){
        const put = await instanceAxios.put(url, data?.arg).then(res => res.data)
        .catch(function(err){
            toast.error(err?.response?.data?.message)
        })
        console.log(url, data)
        if(put){
            toast.success(put?.message)
            router.push('/jadwal')    
        }
    }
    const urlPut = 'mahasiswa/jadwal/'+id;
    const { trigger, isMutating } = useSWRMutation(urlPut, sendReq)
    
    const { register, handleSubmit, formState: { errors } } = useForm();

    const onSubmit = data => trigger(data)
    //END PUT
    
    //GET
    const fetcher = (url) => instanceAxios.get(url).then(res => res.data)
    const address = 'mahasiswa/jadwal/' + id;
    const { data, error } = useSWR(address, fetcher);
    if (error){
        return <p>Loading failed...</p>;
    }
    if (!data){
        return <h1>Loading...</h1>;
    }
    //END GET

    return (
        <div>
            <h1 className="text-2xl font-bold leading-7">Update Jadwal</h1>
            
            <form onSubmit={handleSubmit(onSubmit)}>
                <div className="grid grid-cols-1 md:grid-cols-3 gap-4">
                    <div className="form-control">
                        <label className="label">
                            <span className="label-text">*Semester:</span>
                        </label>
                        <select defaultValue={data?.semester} className="input input-bordered" {...register("semester", { required: true })}>
                            <option value="">Pilih..</option>
                            <option value="1">SEMESTER 1</option>
                            <option value="2">SEMESTER 2</option>
                            <option value="3">SEMESTER 3</option>
                            <option value="4">SEMESTER 4</option>
                            <option value="5">SEMESTER 5</option>
                            <option value="6">SEMESTER 6</option>
                            <option value="7">SEMESTER 7</option>
                            <option value="8">SEMESTER 8</option>
                        </select>
                        {errors.semester?.type === 'required' && <span className="text-red-600">Wajib diisi</span>}
                    </div>

                    <div className="form-control">
                        <label className="label">
                            <span className="label-text">*Dosen:</span>
                        </label>
                        <input defaultValue={data?.dosen} {...register("dosen", { required: true })}
                            type="text" placeholder="Dosen" className="input input-bordered" />
                        {errors.dosen?.type === 'required' && <span className="text-red-600">Wajib diisi</span>}
                    </div>

                    <div className="form-control">
                        <label className="label">
                            <span className="label-text">*Hari:</span>
                        </label>
                        <select defaultValue={data?.hari} className="input input-bordered" {...register("hari", { required: true })}>
                            <option value="">Pilih..</option>
                            <option value="SENIN">SENIN</option>
                            <option value="SELASA">SELASA</option>
                            <option value="RABU">RABU</option>
                            <option value="KAMIS">KAMIS</option>
                            <option value="JUMAT">JUMAT</option>
                        </select>
                        {errors.hari?.type === 'required' && <span className="text-red-600">Wajib diisi</span>}
                    </div>

                    <div className="form-control">
                        <label className="label">
                            <span className="label-text">*Jam Mulai:</span>
                        </label>
                        <input defaultValue={data?.mulai} {...register("mulai", { required: true })}
                            type="text" placeholder="00:00" className="input input-bordered" />
                        {errors.mulai?.type === 'required' && <span className="text-red-600">Wajib diisi</span>}
                    </div>

                    <div className="form-control">
                        <label className="label">
                            <span className="label-text">*Jam Selesai:</span>
                        </label>
                        <input defaultValue={data?.selesai} {...register("selesai", { required: true })}
                            type="text" placeholder="00:00" className="input input-bordered" />
                        {errors.selesai?.type === 'required' && <span className="text-red-600">Wajib diisi</span>}
                    </div>

                    <div className="form-control">
                        <label className="label">
                            <span className="label-text">*Mata Kuliah:</span>
                        </label>
                        <input defaultValue={data?.mata_kuliah} {...register("mata_kuliah", { required: true })}
                            type="text" placeholder="Mata Kuliah" className="input input-bordered" />
                        {errors.mata_kuliah?.type === 'required' && <span className="text-red-600">Wajib diisi</span>}
                    </div>

                    <div className="form-control">
                        <label className="label">
                            <span className="label-text">Nama PJM:</span>
                        </label>
                        <input defaultValue={data?.pjm} {...register("pjm")}
                            type="text" placeholder="Nama PJM" className="input input-bordered" />
                    </div>
                </div>

                {isMutating ? 'Sending...' : <input className="btn btn-neutral btn-sm m-2" type="submit" />}
            </form>
        </div>
    )
}
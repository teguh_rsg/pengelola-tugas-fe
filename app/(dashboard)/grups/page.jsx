"use client"

import { createColumnHelper } from '@tanstack/react-table'
import { useMemo, useState } from 'react'
import useSWR, { useSWRConfig } from 'swr'
import instanceAxios from '@/axios/instance';
import TableInstant from '@/components/TableInstant';
import Link from "next/link";
import useSWRMutation from 'swr/mutation';
import Swal from 'sweetalert2';
import { toast } from 'react-toastify';
import { useRouter } from 'next/navigation';

export default function Grups() {
    const columnHelper = createColumnHelper()

    const [{ pageIndex, pageSize }, setPagination] = useState({
        pageIndex: 0,
        pageSize: 10,
    })

    const fetcher = (url) => instanceAxios.get(url).then(res => res.data)
    const address = 'admin/grup?limit=' + pageSize + '&skip=' + (pageIndex * pageSize);

    const columns = useMemo(() => [
        columnHelper.accessor('id', {
            header: () => 'Opsi',
            cell: info => <>
                <Link href={{
                    pathname: "/grups/update",
                    query: { id: info.row.original.id },
                }} className="btn btn-square btn-sm btn-warning m-1">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-pencil-fill" viewBox="0 0 16 16">
                        <path d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z" />
                    </svg>
                </Link>

                <ButtonHapus address={address} rowId={info.row.original.id} />
            </>,
        }),
        columnHelper.accessor('name'),
        columnHelper.accessor('code', {
            header: () => <span>Code</span>,
        }),
        columnHelper.accessor('created_at', {
            header: () => <span>Created At</span>,
        }),
    ], [])

    const { data: dataGet, error, isLoading, isValidating } = useSWR(address, fetcher);

    if (error) {
        return <p>Loading failed...</p>;
    }

    const dataList = dataGet?.data ? dataGet.data : [];
    return <>
        <Link href="/grups/create" className="btn btn-primary m-1">Tambah</Link>

        <TableInstant
            data={dataList}
            columns={columns}
            setPagination={setPagination}
            isLoading={isLoading}
            isValidating={isValidating}
            pageCount={dataGet?.total && Math.ceil(dataGet.total / pageSize)}
            pageSize={pageSize}
            pageIndex={pageIndex}
        />
    </>

}

function ButtonHapus({ address, rowId }) {
    const { mutate } = useSWRConfig()
    const router = useRouter()

    function confirm() {
        Swal.fire({
            title: 'Delete?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
        }).then((result) => {
            if (result.isConfirmed) {
                trigger()
            }
        })
    }

    async function sendReq(url) {
        const result = await instanceAxios.delete(url).then(res => res.data)
            .catch(function (err) {
                toast.error(err?.response?.data?.message)
            })
        console.log(url)
        if (result) {
            toast.success(result?.message)
            mutate(address)
        }
    }
    const addressDel = 'admin/grup/' + rowId;
    const { trigger, isMutating } = useSWRMutation(addressDel, sendReq)

    if (isMutating) {
        return 'deleting...'
    }

    return <button onClick={confirm} className="btn btn-square btn-sm btn-error m-1">
        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-trash-fill" viewBox="0 0 16 16">
            <path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5M8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5m3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0" />
        </svg>
    </button>
}
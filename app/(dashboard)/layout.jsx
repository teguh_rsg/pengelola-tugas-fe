"use client"

import Link from "next/link";
import { useEffect, useState } from "react";
import useSidebarStore from "@/zustand/sidebar"
import { useRouter } from "next/navigation";
import SidebarMenu from "./SidebarMenu";

function logout() {
    window.localStorage.removeItem("jwtToken")
    window.location.href = '/login/'
}

function Navbar() {
    const toggle = useSidebarStore((state) => state.toggle)

    return (
        <div className="navbar bg-gray-800 text-slate-50 drop-shadow-md sticky top-0 z-20">
            <div className="flex-none">
                <button onClick={toggle} className="btn btn-square btn-ghost">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" className="inline-block w-5 h-5 stroke-current"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M4 6h16M4 12h16M4 18h16"></path></svg>
                </button>
            </div>
            <div className="flex-1">
                <a className="btn btn-ghost normal-case text-xl">Dashboard</a>
            </div>
            <div className="flex-none">
                <div className="dropdown dropdown-end">
                    <label tabIndex={0} className="btn btn-square btn-ghost">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" className="inline-block w-5 h-5 stroke-current"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M5 12h.01M12 12h.01M19 12h.01M6 12a1 1 0 11-2 0 1 1 0 012 0zm7 0a1 1 0 11-2 0 1 1 0 012 0zm7 0a1 1 0 11-2 0 1 1 0 012 0z"></path></svg>
                    </label>
                    <ul tabIndex={0} className="dropdown-content menu p-2 shadow bg-gray-900 text-slate-50 rounded-box w-52">
                        <li><Link href="/account/password">Ganti Password</Link></li>
                        <li><button onClick={logout}>Logout</button></li>
                    </ul>
                </div>
            </div>
        </div>
    )
}

function BelumLogin() {
    const [tampil, setTampil] = useState(false)
    useEffect(() => {
        const timer = setTimeout(() => {
            console.log('Tampil Belum Login')
            setTampil(true);
        }, 5000);

        return () => clearTimeout(timer);
    }, []);
    
    if(!tampil){
        return 'Loading......'
    }

    return <div className="m-2">
        <h1 className="text-2xl">Belum login</h1>
        <p>Silahkan login terlebih dahulu</p>
        <Link href="/login" className="btn btn-neutral btn-sm">Login</Link>
    </div>
}

export default function Layout({ children }) {
    const { isActive, toggle, setActive } = useSidebarStore((state) => state)
    const router = useRouter()

    const [login, setLogin] = useState(false)

    useEffect(function () {
        if (!window.localStorage.getItem("jwtToken")) {
            router.push('/login')
        } else {
            setLogin(true)
        }
        setActive(window.innerWidth >= 768)
    }, [])

    if (!login) {
        return <BelumLogin />
    }

    let sidebarClass = 'bg-gray-900 text-slate-50 h-screen overflow-x-auto fixed z-30 w-full '
    sidebarClass += isActive ? 'sm:w-64' : 'sm:w-16 hidden sm:block'

    let contentClass = 'w-full '
    contentClass += isActive ? 'sm:pl-64' : 'sm:pl-16'

    let footerClass = 'mt-auto '
    footerClass += isActive ? 'sm:pl-64' : 'sm:pl-16'

    return (
        <div className="flex flex-col min-h-screen">
            <div className={sidebarClass}>

                <button onClick={toggle} className="btn btn-square btn-sm fixed right-0 z-10 m-1 sm:hidden">
                    <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M6 18L18 6M6 6l12 12" /></svg>
                </button>

                <SidebarMenu />

            </div>

            <div className={contentClass}>
                <Navbar />
                <div className="m-2">
                    {children}
                </div>
            </div>

            <div className={footerClass}>
                {/* <Footer/> */}
            </div>

        </div>
    )
}
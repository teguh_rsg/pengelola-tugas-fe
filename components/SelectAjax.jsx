"use client"

import useSWR from 'swr';
import Select from 'react-select';
import { useEffect, useState } from 'react';
import instanceAxios from "@/axios/instance"

export default function SelectAjax(props) {
    const [search, setSearch] = useState('')
    const [input, setInput] = useState('')
    const fetcher = (url) => instanceAxios.get(url).then(res => res.data)
    const address = props.createAddress(search)

    let timer
    useEffect(() => {
        clearTimeout(timer)
        timer = setTimeout(() => {
            console.log(input)
            setSearch(input);
        }, 500);

        return () => clearTimeout(timer);
    }, [input]);
    
    /** contoh createAddress adalah..  (search)=> 'mahasiswa/jadwal?search=' + search; */
    const { data, error } = useSWR(address, fetcher);
    if (error) {
        return <p>Loading failed...</p>
    }

    const options = typeof props.optionsMap === 'function' ? props.optionsMap(data) : data
    /** contoh data dari respon server adalah.. [{ value = 1, label: bla }] */
    const label = props.labelDefaultValue ? props.labelDefaultValue : (props.field.value ? props.field.value : null)
    const defaultValue = props.field.value ? {value:props.field.value, label:label} : null

    return <Select
        ref={props?.selectInputRef}
        isClearable
        onChange={(val)=>props.field.onChange(val?.value)}
        options={options}
        // inputValue={input}
        onInputChange={(value, action) => {
            if (action?.action !== 'input-blur' && action?.action !== 'menu-close') {
                setInput(value);
            }
        }}
        isLoading = {!data}
        defaultValue={ defaultValue }
        className="z-[12] text-black" />
}
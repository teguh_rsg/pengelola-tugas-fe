import axios from "axios";

const instanceAxios = axios.create({
    baseURL: process.env.NEXT_PUBLIC_BE_API_BASE_URL,
    timeout: 1000 * 60,
});

console.log('baseURL :: ' + process.env.NEXT_PUBLIC_BE_API_BASE_URL)

// Add a request interceptor
instanceAxios.interceptors.request.use(function (config) {
    // Do something before request is sent
    if (window.localStorage.getItem("jwtToken")) {
        config.headers.Authorization = 'Bearer ' + window.localStorage.getItem("jwtToken");
    }
    return config;
}, function (error) {
    // Do something with request error
    return Promise.reject(error);
});

// Add a response interceptor
instanceAxios.interceptors.response.use(function (response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    return response;
}, function (error) {
    if (error.response.request.status === 401) {
        //401 = belum login atau harus login ulang karena token sudah tidak valid
        alert(error.response.request.status + ' | ' + error.response?.data?.message)
        window.localStorage.removeItem("jwtToken");
        window.location.href = '/login' //BELUM MENEMUKAN ALTERNATIF SELAIN window.location. 
    }
    else if (error.response.request.status === 403) {
        //403 = sudah login tapi hak akses tidak sesuai
        alert(error.response.request.status + ' | ' + error.response?.data?.message)
        window.location.href = '/akses-ditolak' //BELUM MENEMUKAN ALTERNATIF SELAIN window.location. 
    }
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    return Promise.reject(error);
});

export default instanceAxios;